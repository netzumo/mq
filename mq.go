package mq

import (
	"time"

	"bitbucket.org/netzumo/log"
	"github.com/nats-io/nats"
)

// RunMode constants
const (
	DEV  string = "development"
	PROD string = "production"
)

var (
	env   = DEV
	nConn *nats.EncodedConn
)

// NatsCfg is nats config struct
type NatsCfg struct {
	Servers       []string
	MaxReconnect  int
	ReconnectWait time.Duration
	Timeout       time.Duration
	PingInterval  time.Duration
	MaxPingsOut   int
}

// Publish will publish a payload with topic
func Publish(topic string, payload interface{}) error {
	log.Infof("publish %s with topic %s", payload, topic)
	err := nConn.Publish(topic, payload)
	if err != nil {
		return err
	}

	return nil
}

// Request perform rpc style call
func Request(subj string, requestObj interface{}, responseObj interface{}, timeout time.Duration) error {
	log.Infof("request %s with subject %s", requestObj, subj)
	return nConn.Request(subj, requestObj, responseObj, timeout)
}

// RPCSubscribe subscribe to rpc call subject
func RPCSubscribe(subj string, cb interface{}) (sub *nats.Subscription, err error) {
	log.Infof("subscribe rpc with subject %s", subj)
	sub, err = nConn.Subscribe(subj, cb)
	return
}

// EventSubscribe subscribe to event topic
func EventSubscribe(topic string, cb interface{}) (sub *nats.Subscription, err error) {
	log.Infof("subscribe event with subject %s", topic)
	sub, err = nConn.Subscribe(topic, cb)
	return
}

// QueueSubscribe subscribe to queue topic
func QueueSubscribe(topic, queue string, cb interface{}) (sub *nats.Subscription, err error) {
	log.Infof("subscribe queue with topic %s", topic)
	sub, err = nConn.QueueSubscribe(topic, queue, cb)
	return
}

// Close close the mq connection
func Close() {
	log.Info("Closing MQ connection")
	nConn.Close()
}

// Init will load the specified postgres addr
func Init(cfg *NatsCfg, envrmt string) error {
	env = envrmt

	opts := nats.DefaultOptions
	opts.Servers = cfg.Servers
	opts.MaxReconnect = cfg.MaxReconnect
	opts.ReconnectWait = cfg.ReconnectWait
	opts.Timeout = cfg.Timeout
	opts.PingInterval = cfg.PingInterval
	opts.MaxPingsOut = cfg.MaxPingsOut

	conn, err := opts.Connect()
	if err != nil {
		return err
	}

	conn.Opts.DisconnectedCB = func(nc *nats.Conn) {
		log.Warnf("Got disconnected! Reconnects: %d", nc.Reconnects)
	}

	conn.Opts.ClosedCB = func(_ *nats.Conn) {
		log.Error("Connection to nats servers is closed")
	}

	nConn, _ = nats.NewEncodedConn(conn, "json")
	return nil
}
